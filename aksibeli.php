<?php

include "koneksi.php";
    $judul = $_POST['judul'];
    $nama = $_POST['nama'];
    $alamat = $_POST['alamat'];
    $no_telp = $_POST['no_telp'];
    $email = $_POST['email'];
    $provinsi = $_POST['provinsi'];
    $kota = $_POST['kota'];
    $jumlah = $_POST['jumlah'];
    $kurir = $_POST['kurir'];
    $berat = 1;
    $kotaasal = 107; //Cimahi
    $harga = 20000;

    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_POSTFIELDS => "origin=$kotaasal&destination=$kota&weight=10&courier=$kurir",
    CURLOPT_HTTPHEADER => array(
        "content-type: application/x-www-form-urlencoded",
        "key: f58bed8eec84ac19923d8964a9afabdf"
    ),
    ));
    

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
    echo "cURL Error #:" . $err;
    } else {
    
    };
    
    $data = json_decode($response, true);



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <h1>Jasa Pengiriman Melaui <?= $kurir ?></h1>
        </div>
        <div class="row">
            <?php for($i=0; $i<count($data["rajaongkir"]["results"][0]["costs"][0])-1; $i++){ ?>
            <div class="card w-50">
                <div class="card-body">
                    <h5 class="card-title"><?= $data["rajaongkir"]["results"][0]["costs"][$i]["service"] ?></h5>
                    <p class="card-text">Ongkos Kirim : Rp.<?= $data["rajaongkir"]["results"][0]["costs"][$i]["cost"][0]["value"] ?></p>
                    <p class="card-text">Total Harga : Rp.<?= $data["rajaongkir"]["results"][0]["costs"][$i]["cost"][0]["value"]+20000;?></p>
                    <a href="#" class="btn btn-primary">Pilih</a>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</body>
</html>

