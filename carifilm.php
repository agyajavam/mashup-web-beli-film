<?php 

 $carifilm = rawurlencode($_POST['namafilm']);

 //Get Data Kabupaten 
  $curl = curl_init();  
  curl_setopt_array($curl, array( 
    CURLOPT_URL => "http://www.omdbapi.com/?apikey=36f559aa&s=$carifilm", 
    CURLOPT_RETURNTRANSFER => true, 
    CURLOPT_ENCODING => "", 
    CURLOPT_MAXREDIRS => 10, 
    CURLOPT_TIMEOUT => 30, 
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, 
    CURLOPT_CUSTOMREQUEST => "GET", 
    CURLOPT_HTTPHEADER => array( 
        
      
    ), 
  )); 

  $response = curl_exec($curl); 
  $err = curl_error($curl); 

  curl_close($curl);
  $data = json_decode($response, true);

  
 
  
  
  
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Beli Film Lengkap</title>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">    
        <a class="navbar-brand" href="#">WPU Movie</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
            <a class="nav-item nav-link active" href="#">Search Movie</a>
            </div>
        </div>
    </div>
  </nav>

    <div class="container">

        <div class="row mt-3 justify-content-center">
            <div class="col-md-8">
                <h1 class="text-center">Search Movie</h1>
                <div class="input-group mb-3">
                <form action="carifilm.php" method="post" class="input-group mb-3">
                    <input type="text" class="form-control" name="namafilm" placeholder="Movie title.." id="search-input">
                    <div class="input-group-append">
                      <button class="btn btn-dark" type="submit" id="search-button">Search</button>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <hr>

        <div class="row" id="movie-list">
            
        </div>


    </div>

    <div class="modal-content">
        <div class="modal-header">
          
        </div>
        <?php for ($i=0; $i < count($data["Search"]); $i++) {  ?>
        <div class="modal-body">
        
            <div class="col-md-4 mx-auto">
                
                <div class="card mb-3 ">
                    <img src="<?= $data["Search"][$i]["Poster"]."<br>"; ?>" class="card-img-top" alt="...">
                    <div class="card-body">
                    <h5 class="card-title"><?= $data["Search"][$i]["Title"]."<br>"; ?></h5>
                    <h6 class="card-subtitle mb-2 text-muted"><?= $data["Search"][$i]["Year"]."<br>"; ?></h6>
                    <a href="detail.php?id=<?= $data["Search"][$i]["imdbID"]; ?>&namafilm=<?= $data["Search"][$i]["Title"]." trailer"; ?>" class="card-link see-detail" data-toggle="modal" data-target="#exampleModal" data-id="${data.imdbID}">See Detail</a>
                    </div>
                </div>
                
            </div>
           
        </div>
        <?php } ?>
      </div>
  
  <!-- Modal -->
    
  
</body>
</html>