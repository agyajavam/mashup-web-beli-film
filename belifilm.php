<?php 
 //Get Data Kabupaten 
  $curl = curl_init();  
  curl_setopt_array($curl, array( 
    CURLOPT_URL => "http://api.rajaongkir.com/starter/city", 
    CURLOPT_RETURNTRANSFER => true, 
    CURLOPT_ENCODING => "", 
    CURLOPT_MAXREDIRS => 10, 
    CURLOPT_TIMEOUT => 30, 
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, 
    CURLOPT_CUSTOMREQUEST => "GET", 
    CURLOPT_HTTPHEADER => array( 
      "Key: f58bed8eec84ac19923d8964a9afabdf" 
    ), 
  )); 

  $response = curl_exec($curl); 
  $err = curl_error($curl); 

  curl_close($curl); 

 
  $datakota = json_decode($response, true); 
  
  //Get Data Kabupaten 


  //----------------------------------------------------------------------------- 

  //Get Data Provinsi 
  $curl = curl_init(); 

  curl_setopt_array($curl, array( 
    CURLOPT_URL => "http://api.rajaongkir.com/starter/province", 
    CURLOPT_RETURNTRANSFER => true, 
    CURLOPT_ENCODING => "", 
    CURLOPT_MAXREDIRS => 10, 
    CURLOPT_TIMEOUT => 30, 
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, 
    CURLOPT_CUSTOMREQUEST => "GET", 
    CURLOPT_HTTPHEADER => array( 
      "Key: f58bed8eec84ac19923d8964a9afabdf" 
    ), 
  )); 

  $response = curl_exec($curl); 
  $err = curl_error($curl); 

  $dataprov = json_decode($response, true); 
  
  

 ?> 

<?php
    $id = $_GET['id'];
    //Get Data Kabupaten 
    $curl = curl_init();  
    curl_setopt_array($curl, array( 
    CURLOPT_URL => "http://www.omdbapi.com/?apikey=36f559aa&i=$id", 
    CURLOPT_RETURNTRANSFER => true, 
    CURLOPT_ENCODING => "", 
    CURLOPT_MAXREDIRS => 10, 
    CURLOPT_TIMEOUT => 30, 
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, 
    CURLOPT_CUSTOMREQUEST => "GET", 
    CURLOPT_HTTPHEADER => array( 
        
      
        ), 
    )); 

  $response = curl_exec($curl); 
  $err = curl_error($curl); 

  curl_close($curl);
  $data = json_decode($response, true);

?>

<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
                <div class="container-fluid">
                    <div class="row">
                        <div>
                            <h1>BELI FILM</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="<?= $data["Poster"]; ?>" class="img-fluid">
                            <h3>Harga : Rp.20.000</h3>
                        </div>
                        <div class="col-md-8">
                            <ul class="list-group">
                            <form action="aksibeli.php" method="post">
                                <li class="list-group-item"><input type="text" name="judul" value="<?= $data["Title"]; ?>" style="width:100%"></li>
                                <li class="list-group-item">Nama Pemesan   : <input type="text" name="nama"></li>
                                <li class="list-group-item">Alamat Lengkap : <textarea type="text" name="alamat"></textarea></li>                 
                                <li class="list-group-item">No Telp        : <input type="text" name="no_telp"></li>         
                                <li class="list-group-item">Email        : <input type="text" name="email"></li>         
                                <li class="list-group-item">Provinsi : 
                                    <select type="text" name="provinsi">
                                        <option>Pilih Provinsi</option>
                                        <?php for ($i=0; $i < count($dataprov['rajaongkir']['results']); $i++) {  ?> 
  
                                        <option value="<?php echo $dataprov['rajaongkir']['results'][$i]['province_id']; ?>"><?php echo $dataprov['rajaongkir']['results'][$i]['province']; ?></option>
                                        <?php }  ?>
                                    </select>
                                </li>    
                                <li class="list-group-item">Kota : 
                                    <select type="text" name="kota">
                                        <option>Pilih Kota</option>
                                        <?php for ($i=0; $i < count($datakota['rajaongkir']['results']); $i++) {  ?> 
  
                                        <option value="<?php echo $datakota['rajaongkir']['results'][$i]['city_id']; ?>"><?php echo $datakota['rajaongkir']['results'][$i]['city_name']; ?></option>
                                        <?php }  ?>
                                    </select>
                                </li>
                                <li class="list-group-item">Jumlah : <input type="text" name="jumlah"></li>
                                <li class="list-group-item">Kurir : 
                                    <select type="text" name="kurir">
                                        <option>Pilih Kurir</option>
                                        <option value="jne">JNE</option>
                                        <option value="pos">POS</option>
                                        <option value="tiki">TIKI</option>
                                    </select>
                                </li> 
                                <li class="list-group-item"><input class="btn btn-primary" type="submit" value="Beli"></li>
                            </form>           
                            </ul>
                        </div>
                        
                    </div>
                      
                </div>
                
            
            
            

</body>
</html>